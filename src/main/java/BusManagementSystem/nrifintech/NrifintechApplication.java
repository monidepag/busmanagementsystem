package BusManagementSystem.nrifintech;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NrifintechApplication {

	public static void main(String[] args) {
		SpringApplication.run(NrifintechApplication.class, args);
	}

}
